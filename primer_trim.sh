#!/bin/bash

#################################################
# Ashok R. Dinasarapu Ph.D			            #		
# Emory University, Dept. of Human Genetics	    #	
# ashok.reddy.dinasarapu at emory.edu		    #		
# Date: 09/07/2020				                #		
#################################################

# primers.txt file
# D005391 NNCTGAGTTGGGTCTTTTA
# D005391 CTCCTCACAGGTGCACTGC
# D005392 NTTCACGCTCAGGAGAAAA
# D005393 GTTGGCATCTTCCTTT
# D005394 CTGGTCTCGCCCCATG
# D005395 CTTCATTGTGCTGGGTG
# D005396 CATAAATAGGTCACAAA
# D005397 CTCAGACCGCGTTCTCTC
# D005398 GGTGTCTTGAGATTATCA
# D005399 GGGGAGTGTCAGCTGGCA


HOME_DIR=/Users/adinasa/test

# STEP 1: Read primer sequences from primers.txt file
# Make primer sequences for search "pattern"
# Replace N with [ATGC]

while read name; do
 B=$(echo $name | cut -d' ' -f2)
 var="$var|$B"
 var="$var|${B//[N]/[ATGC]}" 
done < $HOME_DIR/primers.txt

var="${var:1}"
var1=$"\"$var\""

# STEP 2: Search fastq.gz file 
# For Mac use "zcat <" to read fastq.gz file
# Make each read into a single line
#  "1:N:0:18" is removed from read ID row

zcat < $HOME_DIR/HN1493_S18_R1_001.fastq.gz | \
	head -100 | \
	awk '
		{if(NR%4==1) printf $1"\t"}
		{if(NR%4==2) printf $0"\t"}
		{if(NR%4==3) printf $0"\t"}
		{if(NR%4==0) print $0}' | \
	awk -v pat="$var1" 'match($0, pat){l=RLENGTH; s=RSTART}; {
		if (s == length($1)+2) 
			print $1"\t"substr($2,l+1,length($2))"\t"$3"\t"substr($4,l+1,length($4))
		else
			print $0
	}' | \
	tr -s '\t' '\n'

# Finall, use samtools to convert the filtered data file into gzip compressed data file 